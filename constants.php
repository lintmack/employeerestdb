<?php


define("DIR", __DIR__);
define("DS", DIRECTORY_SEPARATOR);
define("CONTROLLERS", DIR . DS . "Controller");
define("MODELS", DIR . DS . "Model");
define("VIEWS", DIR . DS . "View");

define("AUTOLOAD_CLASSES", array( CONTROLLERS, MODELS, VIEWS));

